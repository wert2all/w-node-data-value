'use strict';

/**
 * The entry point.
 *
 * @module DBConnection
 */
module.exports = require('./src/Data');
module.exports.Interface = require('./src/Interface');
