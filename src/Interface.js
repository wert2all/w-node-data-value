'use strict';
const ThrowableImplementationError = require('w-node-implementation-error');
/**
 * @class WGDataInterface
 * @type {WGDataInterface}
 */
module.exports = class WGDataInterface {

    /**
     * @param key
     * @return {*}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    getData(key) {
        throw new ThrowableImplementationError(this, 'getData');
    }

    /**
     *
     * @param {string} key
     * @param {*} value
     * @return WGDataInterface
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    setData(key, value) {
        throw new ThrowableImplementationError(this, 'setData');
    }

    /**
     * @return {[string]}
     * @abstract
     */
    getKeys() {
        throw new ThrowableImplementationError(this, 'setData');
    }

    /**
     * @abstract
     * @return {{}}
     */
    toHash() {
        throw new ThrowableImplementationError(this, 'setData');
    }

};
