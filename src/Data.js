'use strict';
const WGDataInterface = require('./Interface');

/**
 * @class WGData
 * @type {WGDataInterface}
 * @extends {WGDataInterface}
 */
class WGData extends WGDataInterface {

    /**
     *
     * @param {*} data
     */
    constructor(data) {
        super();
        /**
         *
         * @type {*}
         * @private
         */
        this._data = data;
    }

    /**
     * @param key
     * @return {*}
     * @abstract
     */
    getData(key) {
        if (this._data.hasOwnProperty(key)) {
            return this._data[key];
        }
        return null;
    }

    /**
     *
     * @param {string} key
     * @param {*} value
     * @return WGDataInterface
     */
    setData(key, value) {
        const data = this._data;
        data[key] = value;

        return new WGData(data);
    }

    /**
     * @return {[string]}
     */
    getKeys() {
        return Object.keys(this._data);
    }

    /**
     * @return {{}}
     */
    toHash() {
        return this._data;
    }
}

module.exports = WGData;

